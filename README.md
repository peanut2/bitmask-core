# bitmask-core

`bitmask-core` is a library to handle bootstrap for a LEAP provider in an unified way.

## features

* For the initial boostrap, use DoH or an arbitrary SOCKS5 proxy (for Tor/snowflake).
* Handle a local registry of private bridges.
* Handle a local registry of known introducers.


# Prerequisites

Currently you should have (obfsvpn)[https://0xacab.org/leap/obfsvpn] cloned in the same base directory as bitmask-core (see go.work).


# Compile

```
go build ./cmd/bitmaskcfg
```

# Usage

```
bitmaskcfg is a CLI tool to fetch a working configuration 
from a LEAP VPN service.

This application can be used to generate an openvpn config file,
or to inspect the health of different endpoints.

Usage:
  bitmaskcfg [command]

Available Commands:
  bridge      Manage private bridges
  completion  Generate the autocompletion script for the specified shell
  config      Fetch and handle configuration
  help        Help about any command
  introducer  Manage obfuscated introducers
  locations   List all known locations

Flags:
      --config string   config file
  -v, --verbose         verbose output
  -h, --help            help for bitmaskcfg

Use "bitmaskcfg [command] --help" for more information about a command.
```

## Examples

### Fetching a list of locations 

```
> go run ./cmd/bitmaskcfg --verbose locations ls --host localhost --port 8443 --notls
11:10AM INF Using host: localhost:8443 (ssl=false)
11:10AM DBG Disabling DNS over HTTP (not using SSL)
ID  location     
1   amsterdam    
2   miami        
3   montreal     
4   newyorkcity  
5   paris        
6   seattle 
```

### Picking the best location

This command will first fetch gateways for a subset of locations, ping them and
then evaluate the best one based on the measured latency:

```
> go run ./cmd/bitmaskcfg --verbose locations best --host localhost --port 8443 --notls
11:12AM INF Picking best location...
11:12AM INF Using host: localhost:8443 (ssl=false)
11:12AM DBG Disabling DNS over HTTP (not using SSL)
11:12AM DBG 163.172.211.109: min: 10, loss:0.00
11:12AM DBG 163.172.58.132: min: 18, loss:0.00
11:12AM DBG 198.252.153.109: min: 159, loss:0.00
11:12AM DBG results: 3
11:12AM DBG no-loss: 3
Best: amsterdam (163.172.211.109)
```

### Add an introducer

Add a new introducer:

```
❯ ./bitmaskcfg introducer add litteintroducer "obfsvpnintro://1.2.3.4:8443/?cert=neVUoIX/qGFP59HAz8M5XZAt9vbHIXPx5W68Bo6iZdHaqs9Ec3LTTGV8unhLJslVC0mmGA&kcp=1&fqdn=menshen.asdf.network"
```

### Use the introducer to communicate with the API

After successfully adding an introducer to the database, it can be used to contact the API:

```
❯ ./bitmaskcfg introducer ls
ID  Selected  Name              Created       Last used
11            littleintroducer  1 min ago     never

❯ ./bitmaskcfg locations best --introducer littleintroducer
4:23PM INF Picking best location...
4:23PM INF using host: menshen.example.org
4:23PM WRN --host=menshen.example.org will be overriden with --fqdn=other.example.org because introducer has precedence
4:23PM INF Using obfuscated http client
Best: paris (163.172.83.106)
```

# Persistent config

You can use `$HOME/.bitmaskcfg` for some common settings that will be picked by default:

```
❯ cat ~/.bitmaskcfg
host: menshen.example.org
#bridge: bridge-1
#introducer: littleintroducer
```


# bitmask-linux tool

Additionally, refer to the documentation in `cmd/bitmask-linux` for an
experimental linux client using `openvpn3` and `dbus`.


# Maintenance

For synchronized changes in the api, you need to install [swag](https://github.com/swaggo/swag) and use `make` to generate the Swagger client:

```
go install github.com/swaggo/swag/cmd/swag@latest
make client
```
