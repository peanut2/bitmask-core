/*
Copyright © 2023 atanarjuat atanarjuat@riseup.net
Copyright © 2023 LEAP Encryption Access Project

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bitmask-linux",
	Short: "An experimental bitmask wrapper around openvpn3-linux",
	Long: `A thin wrapper around the openvpn3-linux commands that allows
to create, edit and delete bitmask profiles for OpenVPN. It supports all
the features supported by LEAP VPN services (anonymous credentials, introducers,
private bridges).

This is experimental code, intended to test services. No official support can 
be given for the moment - but feel free to report issues or feature requests
at https://0xacab.org/leap/bitmask-core
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	// TODO: run status
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

type ConfigCLIOptions struct {
	NoTLS bool
	Host  string
	Port  int
}

type parameter string

var (
	paramHost  parameter = "host"
	paramPort  parameter = "port"
	paramNoTLS parameter = "notls"
)

type cmdInfo struct {
	Help string
}

var allParams = map[parameter]cmdInfo{
	paramNoTLS: {
		Help: "DANGER: Omit TLS, only for development",
	},
	paramHost: {
		Help: "Host to connect to (default: localhost)",
	},
	paramPort: {
		Help: "Port to connect to (default: 443)",
	},
}

var clicfg = &ConfigCLIOptions{}

func addNoTLSParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().BoolVar(
		&(clicfg.NoTLS),
		name,
		false,
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}

}

func addHostParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().StringVar(
		&(clicfg.Host),
		name,
		"localhost",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

func addPortParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().IntVar(
		&(clicfg.Port),
		name,
		443,
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

func init() {

	cobra.OnInitialize(func() {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	})
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.bitmaskcfg)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	addHostParam(newCmd, paramHost)
	addPortParam(newCmd, paramPort)
	addNoTLSParam(newCmd, paramNoTLS)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".bitmask-linux" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".bitmaskcfg")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Info().Msg(fmt.Sprintf("Using config file: %s", viper.ConfigFileUsed()))
	}
}
