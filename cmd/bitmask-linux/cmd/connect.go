/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"sync"

	"0xacab.org/leap/bitmask-core/pkg/localproxy"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// connectCmd represents the connect command
var connectCmd = &cobra.Command{
	Use:   "connect",
	Short: "Connect to a given tunnel",
	Long: `Start an openvpn3 session for a previously created profile.

If the configured profile uses a bridge, this command will start the bridge
in the background`,
	Args: cobra.MatchAll(cobra.ExactArgs(3), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]

		// TODO get by bridge name
		bridge := args[1]
		cert := args[2]

		// This is a quick-n-dirty way of synchronizing the proxy and the openvpn session.
		var wg sync.WaitGroup

		wg.Add(1)
		go func() {
			if err := localproxy.StartLocalProxy(bridge, cert); err != nil {
				log.Fatal().Err(err).Msg("")
			}

		}()

		wg.Add(1)
		go func() {
			cmdArgs := []string{"session-start", "--config", name}
			if err := runOpenVPNCmd(cmdArgs); err != nil {
				log.Fatal().Err(err).Msg("")
			}
		}()

		// We will block here - if all is good, openvpn should connect through the proxy.
		// When the user sends a SIGINT/SIGTERM, we'll exit - and that will stop the proxy process.
		// User will have to manually issue the "disconnect" command. We could also do it here
		// before exit, but I think it's "safer" to force an intentionall teardown.

		// In any case, if we ever plan on use this for something more than demo purposes,
		// it'll be a good idea to have a more robust system. One such idea could be to run the same
		// binary and detach ourselves, keeping the PID of the proxy process.

		// Or we could just send ourselves to the background here, and making sure we kill the
		// background process when issuing a disconnect.

		// Another idea might be to invert control: if openvpn3 is really able to start early in the system,
		// we might want to add ourselves as an entrypoint for pre-up scripts (bitmask-linux localproxy <bridgename>).
		// that could run as its own user.

		wg.Wait()
	},
}

func init() {
	rootCmd.AddCommand(connectCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// connectCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// connectCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
