/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"0xacab.org/leap/bitmask-core/models"
	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
	"0xacab.org/leap/bitmask-core/pkg/storage"
)

var (
	LocalProxyPort = "8080"
)

// newCmd represents the new command
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Create new tunnel",
	Long: `Fetch a working configuration, with optional circumvention methods.
After downloading the config file, it will register a new configuration 
profile with the openvpn3 daemon.`,
	Args: cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		// We're passing the name for now because it makes it easier to handle.
		// To operate properly, we need to store configuration profiles in some
		// persistent structure, that is able to map the openvpn config name
		// to our bridges and locations.
		name := args[0]
		if err := tunnelNew(name); err != nil {
			log.Fatal().Err(err).Msg("")
		}
	},
}

// TODO - pass --persist to the creation

func init() {
	tunnelCmd.AddCommand(newCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// newCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// newCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// tunnelNew will fetch an openvpn configuration
func tunnelNew(profileName string) error {
	host := viper.GetString("host")
	port := viper.GetInt("port")
	introd := storage.MaybeGetIntroducerURLByName(viper.GetString("introducer"))
	proxy := viper.GetString("proxy")
	noTLS := viper.GetBool(string(paramNoTLS))

	log.Info().Msg(fmt.Sprintf("using host: %s:%d (tls=%v)", host, port, !noTLS))

	cfg := bootstrap.NewConfig()
	cfg.Host = host
	cfg.Port = port
	cfg.Introducer = introd
	cfg.Proxy = proxy
	cfg.UseTLS = !noTLS

	api, err := bootstrap.NewAPI(cfg)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not initialize API")
	}
	// TODO pass cc override
	err = api.DoGeolocationLookup()
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not do geolocation lookup")
	}

	location := viper.GetString("location")

	params := &bootstrap.GatewayParams{}
	params.Location = location
	params.Port = "1194"
	params.Transport = "udp"

	vpnCfg, err := api.SerializeConfig(params)
	if err != nil {
		return err
	}

	tmpDir := "/dev/shm/bitmask/"
	tmpFile := filepath.Join(tmpDir, "bitmask.ovpn")

	if err := os.MkdirAll(tmpDir, os.ModePerm); err != nil {
		return err
	}
	if err := os.WriteFile(tmpFile, []byte(vpnCfg), 0600); err != nil {
		return err
	}

	log.Info().Msg("file written to " + tmpFile)

	bridge := viper.GetString("bridge")
	if bridge != "" {
		b, err := storage.MaybeGetBridgeByName(bridge)
		if err != nil {
			return err
		}
		var bridge models.ModelsBridge
		if err := json.Unmarshal([]byte(b.Raw), &bridge); err != nil {
			return err
		}
		log.Info().Msg(
			fmt.Sprintf("Using configured bridge for this connection: %s (%s)", bridge.Host, bridge.Location))
		log.Debug().Msg("type: " + bridge.Type)
		log.Debug().Msg("ip:" + bridge.IPAddr)
		log.Debug().Msg(fmt.Sprintf("port: %d", bridge.Port))

		// TODO: fix type check when we know how to support more bridges (kcp, hop)
		if bridge.Type != "obfs4" {
			return fmt.Errorf("bridge not supported: %s", bridge.Type)
		}

		options := bridge.Options.(map[string]interface{})
		log.Debug().Msg(fmt.Sprintf("cert: %v", options["cert"]))

		if err := patchConfigFileWithLocalBridge(tmpFile, bridge.Host, bridge.Location, bridge.IPAddr); err != nil {
			log.Fatal().Err(err).Msg("")
		}

		// If we've arrived here, we should be ready to import the config.
		// This will fail if the config name already exists. See note at the beginning
		// of the function for a better way to handle names internally.

		cmdArgs := []string{"config-import", "--config", tmpFile, "--name", profileName}
		if err := runOpenVPNCmd(cmdArgs); err != nil {
			log.Fatal().Err(err).Msg("")
		}
		log.Info().Msg("OpenVPN profile successfully imported: " + profileName)

		// remove temporal config file.
		_ = os.Remove(tmpFile)
	}
	return nil
}

// this function is a workaround for the fact that we cannot generate
// proper local remotes on the menshen side. so we need to replace the remote placeholders
// with the properly configured local proxy (obfs4 or any other bridge).
func patchConfigFileWithLocalBridge(path string, name, location, ipaddr string) error {
	// we want to point to the local proxy, always in UDP mode.
	// besides, we need to make sure that the route to the upstream bridge is always
	// added via the default network gateway.
	bridgeLines := fmt.Sprintf(
		"remote 127.0.0.1 %s udp4\n# static route to bridge %s (%s)\nroute %s 255.255.255.255 net_gateway\n",
		LocalProxyPort,
		name, location,
		ipaddr,
	)

	input, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	// for now we want to replace only the first remote line, and ignore all the rest
	replacedOnce := false
	for i, line := range lines {
		// the space is vital, otherwise we'll match other directives (like remote-cert-tls)
		// we could better match for a pattern like <remote IP PORT udp>.
		if strings.HasPrefix(line, "remote ") {
			if !replacedOnce {
				lines[i] = bridgeLines
				replacedOnce = true
			} else {
				// comment all the remote lines under the fist one
				lines[i] = "# " + line
			}

		}
	}
	output := strings.Join(lines, "\n")
	err = os.WriteFile(path, []byte(output), 0644)
	if err != nil {
		return err
	}
	return nil
}
