package cmd

import (
	"fmt"
	"os/exec"
)

var openvpn = "openvpn3"

// runOpenVPNCmd executes openvpn3 cli with the passed arguments
func runOpenVPNCmd(args []string) error {
	cmd := exec.Command(openvpn, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("cannot start session: %w", err)
	}
	fmt.Println(string(out))
	return nil
}
