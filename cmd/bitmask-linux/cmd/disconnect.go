/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// disconnectCmd represents the disconnect command
var disconnectCmd = &cobra.Command{
	Use:   "disconnect",
	Short: "Disconnect a running session",
	Long:  `...`,
	Args:  cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		cmdArgs := []string{"session-manage", "--disconnect", "--config", name}
		if err := runOpenVPNCmd(cmdArgs); err != nil {
			log.Fatal().Err(err).Msg("")
		}
	},
}

func init() {
	rootCmd.AddCommand(disconnectCmd)
}
