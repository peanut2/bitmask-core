# bitmask-linux

`bitmask-linux` is a thin wrapper around the newer [openvpn3-linux](https://github.com/OpenVPN/openvpn3-linux) project.

LEAP does not support this client officially for the time being, but let us know if it's somehow useful to you.

## Pre-requisites

* `openvpn`. [AUR](https://aur.archlinux.org/packages/openvpn3)

## Usage

`bitmask-linux` uses the same config file than the `bitmaskcfg` utility (by default, this is `$HOME/.bitmaskcfg`). In fact,
you should use `bitmaskcfg` to configure your private bridges, introducers etc. After the local storage knows about these
facts, you can simply pass flags or add the correct values in `~/.bitmaskcfg` for it to pick a bridge or introducer.

### config bridges, proxy etc

```
# ~/.bitmaskcfg
host: vpn.example.org
bridge: bridge-1 # this can be a real bridge host from the public listing, or a host you've introduced in your list of private bridges.
# socks-proxy: localhost:9050 # uncomment to use Tor for boostrap
# introducer: my-secret-introducer # uncomment if you have an usable introducer
```

### create new tunnel

```
❯ ./bitmask-linux tunnel new leap-vpn-bridge-amsterdam-1
9:54PM INF Using config file: /home/user/.bitmaskcfg
9:54PM INF using host: vpn.example.com
9:54PM INF Using uTLS client with direct connection
9:54PM INF Using DoH resolver: 208.67.222.222
9:54PM INF file written to /dev/shm/bitmask/bitmask.ovpn
9:54PM INF Using configured bridge for this connection: bridge-1 (amsterdam)
Configuration imported.  Configuration path: /net/openvpn/v3/configuration/ee476783x7862x49f1xab9ax4599785092a4
9:54PM INF OpenVPN profile successfully imported: leap-vpn-bridge-amsterdam-1
```

## connect to new tunnel

```
❯ ./bitmask-linux connect leap-vpn-bridge-amsterdam-1
9:55PM INF Using config file: /home/user/.bitmaskcfg
Using pre-loaded configuration profile 'leap-vpn-bridge-amsterdam-1'
Session path: /net/openvpn/v3/sessions/816f830ds2d72s43bes8c2fs746ec26e55c8
Connected
```

## status

```
❯ ./bitmask-linux status leap-vpn-bridge-amsterdam-1
9:56PM INF Using config file: /home/user/.bitmaskcfg

Connection statistics:
     BYTES_IN..................566853
     BYTES_OUT.................101259
     PACKETS_IN...................516
     PACKETS_OUT..................567
     TUN_BYTES_IN...............85322
     TUN_BYTES_OUT.............552369
     TUN_PACKETS_IN...............558
     TUN_PACKETS_OUT..............508
```


## disconnect tunnel

```
❯ ./bitmask-linux disconnect leap-vpn-bridge-amsterdam-1
9:56PM INF Using config file: /home/user/.bitmaskcfg
Initiated session shutdown.

Connection statistics:
     BYTES_IN.................1215022
     BYTES_OUT.................199291
     PACKETS_IN..................1237
     PACKETS_OUT.................1177
     TUN_BYTES_IN..............168714
     TUN_BYTES_OUT............1183234
     TUN_PACKETS_IN..............1168
     TUN_PACKETS_OUT.............1229
```

# Tips 

You might want to benefit of the openvpn-dco kernel module [AUR](https://aur.archlinux.org/packages/openvpn-dco-dkms). Let us know
if you perform any interesting benchmark.

