/*
Copyright © 2023 Atanarjuat The Fast Runner <atanarjuat@riseup.net>
*/
package main

import "0xacab.org/leap/bitmask-core/cmd/bitmaskcfg/cmd"

func main() {
	cmd.Execute()
}
