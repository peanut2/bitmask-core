/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/rs/zerolog/log"

	"0xacab.org/leap/bitmask-core/pkg/introducer"
	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/spf13/cobra"
)

// introducerAddCmd represents the list command
var introducerAddCmd = &cobra.Command{
	Use:   "add [name] [url]",
	Short: "Add new introducer",
	Args:  cobra.MatchAll(cobra.ExactArgs(2), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		url := args[1]
		err := addNewIntroducer(name, url)
		if err != nil {
			log.Error().Msg(err.Error())
			os.Exit(1)
		}
	},
}

func addNewIntroducer(name, url string) error {
	_intro, err := introducer.NewIntroducerFromURL(url)
	if err != nil {
		return err
	}
	if err := _intro.Validate(); err != nil {
		return fmt.Errorf("bad introducer: %v", err)
	}
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		log.Fatal().Err(err)
	}
	defer db.Close()

	// It's important that we use the canonical URL instead of the one
	// that we've been passed, to ensure uniqueness when looking up.
	if err := db.NewIntroducer(name, _intro.URL()); err != nil {
		return fmt.Errorf("cannot save: %v", err)
	}
	return nil
}
