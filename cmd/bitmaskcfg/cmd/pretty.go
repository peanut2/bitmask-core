package cmd

import (
	"fmt"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/rs/zerolog/log"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
)

var (
	headerFmt = color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt = color.New(color.FgYellow).SprintfFunc()
)

func yesNoPrompt() bool {
	prompt := promptui.Select{
		Label: "Select[Yes/NO]",
		Items: []string{"NO", "YES"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatal().Err(fmt.Errorf("prompt failed %v", err))
	}
	return result == "YES"
}

func humanTime(t time.Time) string {
	almost := humanize.Time(t)
	switch {
	case almost == "a long while ago":
		return "never"
	}
	return almost
}
