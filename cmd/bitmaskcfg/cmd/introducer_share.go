/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"fmt"
	"strconv"

	"github.com/rs/zerolog/log"
	"github.com/skip2/go-qrcode"
	"github.com/spf13/cobra"

	"0xacab.org/leap/bitmask-core/pkg/storage"
)

// introducerShareCmd represents the introducerShare command
var introducerShareCmd = &cobra.Command{
	Use:   "share",
	Short: "Share an introducer from the database",
	Args:  cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		if err := genQRForIntroducer(args[0]); err != nil {
			log.Fatal().Err(err).Msg("")
		}
	},
}

func genQRForIntroducer(idStr string) error {
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer db.Close()

	id, err := strconv.Atoi(idStr)
	if err != nil {
		return err
	}
	intr, err := db.GetIntroducerByID(id)
	if err != nil {
		return fmt.Errorf("cannot find id %s: %w", idStr, err)
	}

	qrCode, err := qrcode.New(intr.URL, qrcode.Medium)
	if err != nil {
		return fmt.Errorf("could not generate QR code: %v", err)
	}
	fmt.Println()
	fmt.Println("You can share the introducer with this QR code")
	fmt.Println()
	fmt.Println(qrCode.ToSmallString(false))
	// TODO export to PNG too
	return nil
}
