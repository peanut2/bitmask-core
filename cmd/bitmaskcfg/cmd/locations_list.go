/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"slices"
	"sort"

	"github.com/rodaine/table"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
)

// locationsLsCmd represents the locationsLs command
var locationsLsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List all locations",
	Run: func(cmd *cobra.Command, args []string) {
		tbl := table.New("ID", "location")
		tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)
		locations := getLocationList()
		sort.Strings(locations)
		for idx, loc := range locations {
			tbl.AddRow(idx+1, loc)
		}
		tbl.Print()
	},
}

func getLocationList() []string {

	cfg := getConfigFromFlags()

	api, err := bootstrap.NewAPI(cfg)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not initialize API")
	}

	gateways, err := api.GetGateways(nil)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not get gateways from menshen")
	}

	locations := []string{}
	for _, gw := range gateways {
		if !slices.Contains(locations, gw.Location) {
			locations = append(locations, gw.Location)
		}
	}
	return locations
}
