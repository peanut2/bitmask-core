/*
Copyright © Atarnajuat <atanarjuat@riseup.net>
*/
package cmd

import (
	"fmt"
	"os"
	"path"
	"path/filepath"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/fsnotify/fsnotify"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// TODO move somewhere else
	location  = "location"
	port      = "port"
	transport = "transport"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bitmaskcfg",
	Short: "Generates working configuration for Bitmask, the LEAP VPN Client",
	Long: `bitmaskcfg is a CLI tool to fetch a working configuration 
from a LEAP VPN service.

This application can be used to generate an openvpn config file,
or to inspect the health of different endpoints.`,
	//DisableFlagParsing: true,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// Top-level commands

// introducerCmd represents the introducer command
var introducerCmd = &cobra.Command{
	Use:   "introducer",
	Short: "Manage obfuscated introducers",
}

// bridgeCmd represents the bridge command
var bridgeCmd = &cobra.Command{
	Use:   "bridge",
	Short: "Manage private bridges",
	Long: `A private bridge is a bridge that is not publicly listed. 

We may have received this private bridge by an off-band mean (like a
url-encoded string, or a QR), or via an introducer`,
}

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Fetch and handle configuration",
}

// locationsCmd represents the locations command
var locationsCmd = &cobra.Command{
	Use:                "locations",
	Short:              "List all known locations",
	Long:               `Fetches a list of all the locations from the given host.`,
	DisableFlagParsing: false,
}

type ConfigCLIOptions struct {
	Config      string
	CountryCode string
	FromFile    string
	JSON        bool
	Host        string
	Port        int
	Introducer  string
	OutFile     string
	Proxy       string
	Verbose     bool
	NoTLS       bool
}

type parameter string

var (
	paramBridge     parameter = "bridge"
	paramConfig     parameter = "config"
	paramCC         parameter = "cc"
	paramFromFile   parameter = "from-file"
	paramJSON       parameter = "json"
	paramHost       parameter = "host"
	paramPort       parameter = "port"
	paramIntroducer parameter = "introducer"
	paramOut        parameter = "out"
	paramProxy      parameter = "socks5-proxy"
	paramVerbose    parameter = "verbose"
	paramNoTLS      parameter = "notls"
)

type cmdInfo struct {
	Help string
}

var allParams = map[parameter]cmdInfo{
	paramCC: {
		Help: "Use this Country Code (2-letter) when picking endpoints",
	},
	paramConfig: {
		Help: "Host serving the VPN API",
	},
	paramHost: {
		Help: "Host to connect to (default: localhost)",
	},
	paramPort: {
		Help: "Port to connect to (default: 443)",
	},
	paramFromFile: {
		Help: "Load item definition from the specified file path",
	},
	paramJSON: {
		Help: "Return output in json format",
	},
	paramIntroducer: {
		Help: "Use this introducer to contact API",
	},
	paramOut: {
		Help: "Write output to this file",
	},
	paramProxy: {
		Help: "Use this SOCKS5 Proxy to communicate with the server (e. g. localhost:9050)",
	},
	paramNoTLS: {
		Help: "DANGER: Omit TLS, only for development",
	},
}

var clicfg = &ConfigCLIOptions{}

// addHostParam sets the base host
func addHostParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().StringVar(
		&(clicfg.Host),
		name,
		"localhost",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addPortParam sets the base host
func addPortParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().IntVar(
		&(clicfg.Port),
		name,
		443,
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addNoTLSParam sets the base host
func addNoTLSParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().BoolVar(
		&(clicfg.NoTLS),
		name,
		false,
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addIntroducerParam sets an introducer
func addIntroducerParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().StringVar(
		&(clicfg.Introducer),
		name,
		"",
		allParams[param].Help)
	err := viper.BindPFlag(name, locationsCmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addProxyParam sets a socks proxy to fetch data.
func addProxyParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.PersistentFlags().StringVar(
		&(clicfg.Proxy),
		name,
		"",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.PersistentFlags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addCCParam sets the base Country Code
func addCCParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.Flags().StringVar(
		&(clicfg.CountryCode),
		name,
		"",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.Flags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addJSONParam sets the JSON flag
func addJSONParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.Flags().BoolVar(
		&(clicfg.JSON),
		name,
		false,
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.Flags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addFromFileParam sets the fromFile flag
func addFromFileParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.Flags().StringVar(
		&(clicfg.FromFile),
		name,
		"",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.Flags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

// addOutParam sets the outParam flag
func addOutParam(cmd *cobra.Command, param parameter) {
	name := string(param)
	cmd.Flags().StringVar(
		&(clicfg.OutFile),
		name,
		"",
		allParams[param].Help)
	err := viper.BindPFlag(name, cmd.Flags().Lookup(name))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("flag", name).
			Msg("Could not configure command line parameters")
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&clicfg.Config, string(paramConfig), "", "config file")
	rootCmd.PersistentFlags().BoolVarP(&clicfg.Verbose, string(paramVerbose), "v", false, "verbose output")

	cobra.OnInitialize(func() {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		if clicfg.Verbose {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		}
	})

	cobra.OnInitialize(initConfig)

	rootCmd.Flags().SortFlags = false
	rootCmd.PersistentFlags().SortFlags = false

	viper.SetEnvPrefix("BITMASKCFG")
	viper.AutomaticEnv()

	addHostParam(locationsCmd, paramHost)
	addPortParam(locationsCmd, paramPort)
	addNoTLSParam(locationsCmd, paramNoTLS)
	addIntroducerParam(locationsCmd, paramIntroducer)
	addProxyParam(locationsCmd, paramProxy)
	addCCParam(locationsLsCmd, paramCC)
	locationsCmd.AddCommand(locationsLsCmd)
	locationsCmd.AddCommand(locationsBestCmd)

	addHostParam(configFetchCmd, paramHost)
	addPortParam(configFetchCmd, paramPort)
	addProxyParam(configFetchCmd, paramProxy)
	addIntroducerParam(configFetchCmd, paramIntroducer)
	addOutParam(configFetchCmd, paramOut)
	addNoTLSParam(configFetchCmd, paramNoTLS)
	configCmd.AddCommand(configFetchCmd)

	addJSONParam(introducerLsCmd, paramJSON)
	introducerCmd.AddCommand(introducerAddCmd)
	introducerCmd.AddCommand(introducerDeleteCmd)
	introducerCmd.AddCommand(introducerLsCmd)
	introducerCmd.AddCommand(introducerShareCmd)

	addJSONParam(bridgeLsCmd, paramJSON)
	addFromFileParam(bridgeAddCmd, paramFromFile)
	bridgeCmd.AddCommand(bridgeAddCmd)
	bridgeCmd.AddCommand(bridgeDeleteCmd)
	bridgeCmd.AddCommand(bridgeLsCmd)

	rootCmd.AddCommand(configCmd)
	rootCmd.AddCommand(locationsCmd)
	rootCmd.AddCommand(introducerCmd)
	rootCmd.AddCommand(bridgeCmd)
}

func initConfig() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal().Err(err)
	}

	viper.SetConfigName(".bitmaskcfg") // name of config file (without extension)
	viper.SetConfigType("yaml")
	if clicfg.Config != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(clicfg.Config)
		configDir := path.Dir(clicfg.Config)
		if configDir != "." && configDir != dir {
			viper.AddConfigPath(configDir)
		}
	}

	viper.AddConfigPath(dir)
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Debug().Msg(fmt.Sprintf("Using config file: %s", viper.ConfigFileUsed()))
	}
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Debug().Msg(fmt.Sprintf("Config file changed: %s", e.Name))
	})
}

func getConfigFromFlags() *bootstrap.Config {
	host := viper.GetString(string(paramHost))
	port := viper.GetInt(string(paramPort))
	introd := storage.MaybeGetIntroducerURLByName(viper.GetString(string(paramIntroducer)))
	proxy := viper.GetString(string(paramProxy))
	noTLS := viper.GetBool(string(paramNoTLS))

	log.Info().Msg(fmt.Sprintf("Using host: %s:%d (ssl=%v)", host, port, !noTLS))

	cfg := bootstrap.NewConfig()
	cfg.Host = host
	cfg.Port = port
	cfg.Introducer = introd
	cfg.Proxy = proxy
	cfg.UseTLS = !noTLS

	return cfg
}
