/*
Copyright © 2023 atanarjuat <atanarjuat@riseup.net>
*/
package cmd

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
	"0xacab.org/leap/bitmask-core/pkg/latency"
)

// locationsBestCmd represents the location choose command
var locationsBestCmd = &cobra.Command{
	Use:   "best",
	Short: "Pick the best location",
	Long: `Perform a short latency test (ICMP ping) and return the location that seems closer 
to us.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Info().Msg("Picking best location...")
		candidates := getGatewayForEachLocation()
		best, err := pickBest(candidates)
		if err != nil {
			log.Fatal().
				Err(err).
				Msg("Could not pick best location")
		}
		for l, ipaddr := range candidates {
			if best == ipaddr {
				fmt.Printf("Best: %s (%s)\n", l, best)
				break
			}
		}
	},
}

// 1) get all gateways
// 2) for each location, get one (the last gateay)
// 2) pick the gateway with the lowest latency => this is our best location
func getGatewayForEachLocation() map[string]string {

	cfg := getConfigFromFlags()
	api, err := bootstrap.NewAPI(cfg)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not initialize API")
	}
	// TODO get params from cli too, hardcoding this for demo purposes.
	param := &bootstrap.GatewayParams{
		Transport: "udp",
		Port:      "1194",
	}
	gateways, err := api.GetGateways(param)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not get gateways from menshen")
	}

	ips := make(map[string]string)
	for _, gw := range gateways {
		// overwrite / use last one
		ips[gw.Location] = gw.IPAddr
	}
	return ips
}

// pickBest will select the best endpoint by the lower latency.
func pickBest(candidates map[string]string) (string, error) {
	endpoints := []string{}
	for _, ipaddr := range candidates {
		endpoints = append(endpoints, string(ipaddr))
	}

	best, err := latency.PickBestEndpointByLatency(endpoints)
	if err != nil {
		return "", err
	}
	return best, nil
}
