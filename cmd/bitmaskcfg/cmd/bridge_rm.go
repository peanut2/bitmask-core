/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"fmt"
	"os"
	"strconv"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"0xacab.org/leap/bitmask-core/pkg/storage"
)

// bridgeDeleteCmd represents the bridge rm command
var bridgeDeleteCmd = &cobra.Command{
	Use:   "rm [id]",
	Short: "Delete bridge",
	Args:  cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			log.Warn().Msg("TODO: try to parse name")
			os.Exit(1)
		}
		fmt.Printf("Do you really want to remove introducer with ID %d?\n", id)
		if !yesNoPrompt() {
			os.Exit(1)
		}

		err = deleteBridge(id)
		if err != nil {
			log.Error().Err(err).Msg("not ok")
			os.Exit(1)
		}
		log.Info().Msg("bridge rm ok")
	},
}

func deleteBridge(id int) error {
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		return err
	}
	defer db.Close()
	if err := db.DeleteBridge(id, ""); err != nil {
		return fmt.Errorf("cannot delete: %v", err)
	}
	return nil
}
