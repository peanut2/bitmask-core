/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"0xacab.org/leap/bitmask-core/pkg/bootstrap"
)

// configFetchCmd represents the genConfig command
var configFetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "Fetch a working configuration",
	Run: func(cmd *cobra.Command, args []string) {
		generateOpenVPNConfig()
	},
}

func generateOpenVPNConfig() {
	location := viper.GetString(location)
	transport = viper.GetString(transport)
	port := viper.GetString(port)

	cfg := getConfigFromFlags()
	api, err := bootstrap.NewAPI(cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initalize API")
	}

	start := time.Now()

	// begin by attempting to geolocate our base network (CC)
	err = api.DoGeolocationLookup()
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not do geolocation lookup")
	}

	params := &bootstrap.GatewayParams{}
	if location != "" || port != "" || transport != "" {
		params.Location = location
		params.Port = port
		params.Transport = transport
	} else {
		params = nil
	}

	elapsed := time.Since(start)
	log.Info().
		Str("bootstrapSeconds", elapsed.Round(time.Second).String()).
		Msg("Bbootstrap ok")

	vpnCfg, err := api.SerializeConfig(params)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Could not get serialized OpenVPN config")
	}

	out := viper.GetString("out")
	switch {
	case out != "":
		if err := os.WriteFile(out, []byte(vpnCfg), 0600); err != nil {
			log.Fatal().
				Err(err).
				Msg("cannot write file")
		}
		log.Info().
			Str("configFile", out).
			Msg("Sucessfully wrote OpenVPN config file")

	default:
		fmt.Printf("%s\n", vpnCfg)
	}
}
