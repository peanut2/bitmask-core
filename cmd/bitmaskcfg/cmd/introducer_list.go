/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"0xacab.org/leap/bitmask-core/pkg/models"
	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/rodaine/table"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// listCmd represents the list command
var introducerLsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List all introducers",
	Run: func(cmd *cobra.Command, args []string) {
		all := listAllIntroducers()

		switch viper.GetBool(string(paramJSON)) {
		case true:
			out, _ := json.Marshal(all)
			fmt.Println(string(out))

		case false:
			selected := viper.GetString(string(paramIntroducer))
			tbl := table.New("ID", "Selected", "Name", "Created", "Last used")
			tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)
			for _, intr := range all {
				var markSelected string
				if selected == intr.Name {
					markSelected = "✅"
				}
				tbl.AddRow(intr.ID, markSelected, intr.Name, humanTime(intr.CreatedAt), humanTime(intr.LastUsed))
			}
			tbl.Print()
		}
	},
}

func listAllIntroducers() []models.Introducer {
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	all, err := db.ListIntroducers()
	if err != nil {
		log.Fatal(err)
	}
	return all
}
