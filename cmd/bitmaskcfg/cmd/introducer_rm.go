/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"fmt"
	"os"
	"strconv"

	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// introducerDeleteCmd represents the introducer rm command
var introducerDeleteCmd = &cobra.Command{
	Use:   "rm [id]",
	Short: "Delete introducer",
	Args:  cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			log.Warn().Msg("TODO: try to parse name")
			os.Exit(1)
		}
		fmt.Printf("Do you really want to remove introducer with ID %d?\n", id)
		if !yesNoPrompt() {
			os.Exit(1)
		}

		err = deleteIntroducer(id)
		if err != nil {
			log.Error().Err(err).Msg("not ok")
			os.Exit(1)
		}
		log.Info().Msg("introducer rm ok")
	},
}

func deleteIntroducer(id int) error {
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		return err
	}
	defer db.Close()
	if err := db.DeleteIntroducer(id, ""); err != nil {
		return fmt.Errorf("cannot delete: %v", err)
	}
	return nil
}
