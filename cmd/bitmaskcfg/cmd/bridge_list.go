/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"0xacab.org/leap/bitmask-core/pkg/models"
	"0xacab.org/leap/bitmask-core/pkg/storage"
	"github.com/rodaine/table"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// bridgeListCmd represents the list command
var bridgeLsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List all bridges",
	Run: func(cmd *cobra.Command, args []string) {
		all := listAllBridges()

		switch viper.GetBool(string(paramJSON)) {
		case true:
			out, _ := json.Marshal(all)
			fmt.Println(string(out))

		case false:
			selected := viper.GetString(string(paramBridge))
			tbl := table.New("ID", "Selected", "Name", "Type", "Location", "Created", "Last used")
			tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)
			for _, brdg := range all {
				var markSelected string
				if selected == brdg.Name {
					markSelected = "✅"
				}
				tbl.AddRow(brdg.ID, markSelected, brdg.Name, brdg.Type, brdg.Location, humanTime(brdg.CreatedAt), humanTime(brdg.LastUsed))
			}
			tbl.Print()
		}
	},
}

func listAllBridges() []models.Bridge {
	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	all, err := db.ListBridges()
	if err != nil {
		log.Fatal(err)
	}
	return all
}
