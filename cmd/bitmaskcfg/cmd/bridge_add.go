/*
Copyright © 2023 atanarjuat@riseup.net
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	models "0xacab.org/leap/bitmask-core/models"
	"0xacab.org/leap/bitmask-core/pkg/storage"
)

// bridgeAddCmd represents the bridge add command
var bridgeAddCmd = &cobra.Command{
	Use:   "add",
	Short: "Add new bridge",
	Args:  cobra.MatchAll(cobra.ExactArgs(0), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		filePath := viper.GetString(string(paramFromFile))
		if filePath == "" {
			log.Error().Msg("need to pass --from-file")
			os.Exit(1)
		}
		err := addNewBridgeFromFile(filePath)
		if err != nil {
			log.Error().Msg(err.Error())
			os.Exit(1)
		}
	},
}

// addNewBridgeFromFile parses a given file and tries to store the new bridge in the db.
// TODO this function should probably move to pkg/
func addNewBridgeFromFile(filePath string) error {
	bridgeBytes, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	var _brdgs []models.ModelsBridge
	err = json.Unmarshal(bridgeBytes, &_brdgs)
	if err != nil {
		return err
	}

	db, err := storage.NewStorageWithDefaultDir()
	if err != nil {
		return err
	}
	defer db.Close()

	log.Info().Msg(fmt.Sprintf("Got %d bridges", len(_brdgs)))

	for _, b := range _brdgs {
		serialized, _ := json.Marshal(b)
		if err := db.NewBridge(b.Host, b.Type, b.Location, string(serialized)); err != nil {
			return fmt.Errorf("cannot save: %v", err)
		}
	}

	return nil
}
