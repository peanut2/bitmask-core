package localproxy

import (
	"context"

	"0xacab.org/leap/obfsvpn/client"
)

var LocalProxyAddr = "127.0.0.1:8080"

// StartLocalProxy will start a local proxy using the obfsvpn client.
func StartLocalProxy(remoteaddr, obfs4Cert string) error {
	ctx := context.Background()
	// TODO make cancellable

	c := client.NewClient(
		ctx,
		false,
		LocalProxyAddr, // fixed for now, but should make configurable
		obfs4Cert,
	)
	if _, err := c.Start(); err != nil {
		return err
	}
	return nil
}
