package bootstrap

import (
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

func TestValidProxyConfig(t *testing.T) {
	c := getConfigWithoutProxy()
	c.Proxy = "socks5://localhost:9050"
	_, err := NewAPI(c)
	assert.NoError(t, err, "This should be a valid proxy configuration")
}

func TestInvalidProxyConfig(t *testing.T) {
	c := getConfigWithoutProxy()
	c.Proxy = "ThisMissesAScheme"
	_, err := NewAPI(c)
	assert.ErrorContains(t, err, "unknown scheme")
	log.Info().Msgf("%v", err)
}

func getTestAPIWithTorProxy(t *testing.T) *API {
	c := getConfigWithoutProxy()
	c.Proxy = "socks5://localhost:9050"
	api, err := NewAPI(c)
	assert.NoError(t, err, "Could not initialize new API")
	assert.NotNil(t, api, "api should not be nil")
	return api
}
func TestIntegrationGetGatewaysUnfilteredWithTor(t *testing.T) {
	api := getTestAPIWithTorProxy(t)
	_, err := api.GetGateways(nil)
	assert.ErrorContains(t, err, "unknown error general SOCKS serve")
	// TODO: we cant use socks5/tor and connect to a local menshen instance
	// needs a public reachable menshen instance
	/*
		assert.NoError(t, err, "Could not call getGateways")
		assert.NotNil(t, gateways, "gateways should not be nil")
		assert.Greater(t, len(gateways), 3, "there should be at least three gateways returned by menshen")
	*/
}
