package bootstrap

import (
	"os"
	"testing"

	"github.com/rs/zerolog/log"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func getConfigWithoutProxy() *Config {
	c := NewConfig()
	c.Host = "localhost"
	c.Port = 8443
	c.UseTLS = false
	return c
}

func getTestAPI(t *testing.T) *API {
	c := getConfigWithoutProxy()
	api, err := NewAPI(c)
	assert.NoError(t, err, "Could not initialize new API")
	assert.NotNil(t, api, "api should not be nil")
	return api
}

// TODO: rename after menshen enpoint was renamed/restructured
func TestIntegrationGetService(t *testing.T) {
	api := getTestAPI(t)
	service, err := api.GetService()
	assert.NoError(t, err, "Could not call getService")
	assert.NotNil(t, service, "service should not be nil")
}

func TestIntegrationGetGatewaysUnfiltered(t *testing.T) {
	api := getTestAPI(t)
	gateways, err := api.GetGateways(nil)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways returned by menshen")
}

func TestIntegrationGetGatewaysByLocation(t *testing.T) {
	api := getTestAPI(t)
	params := GatewayParams{
		Location: "paris",
	}
	gateways, err := api.GetGateways(&params)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways. Even for location paris")
	for _, gw := range gateways {
		assert.Equal(t, gw.Location, "paris", "this gateway should be located in paris")
	}
}

/*
TODO: There is a logic flaw here: if we ask menshen to give us gateways based on countryCode=US,
this does not mean that the returned gateways are located in the US (only that they are near)
func TestIntegrationGetGatewaysByCountryCode(t *testing.T) {
	api := getTestAPI(t)
	params := GatewayParams{
		CC: "US",
	}
	gateways, err := api.GetGateways(&params)
	assert.NoError(t, err, "Could not call getGateways")
	assert.NotNil(t, gateways, "gateways should not be nil")
	assert.Greater(t, len(gateways), 3, "there should be at least three gateways. Even with country code filtering")
	assert.Fail(t, "menshen needs support to filter for country code")
	for _, gw := range gateways {
		// TODO: Currently, a gateway does not have a field CC. Also the filtering mechanism does not work in menshen
		//assert.Equal(t, gw.Cc, "US", "this gateway should be located in the US")
	}
}
*/

func TestIntegrationGetOpenVPNCert(t *testing.T) {
	api := getTestAPI(t)
	cert, err := api.GetOpenVPNCert()
	assert.NoError(t, err, "Could not call getOpenVPNCert")
	assert.NotNil(t, cert, "cert should not be nil")
	assert.Contains(t, cert, "-----BEGIN RSA PRIVATE KEY-----")
	assert.Contains(t, cert, "-----END RSA PRIVATE KEY-----")
	assert.Contains(t, cert, "-----BEGIN CERTIFICATE-----")
	assert.Contains(t, cert, "-----END CERTIFICATE-----")
}

func TestIntegrationSerializeConfig(t *testing.T) {
	api := getTestAPI(t)
	openVpnConfig, err := api.SerializeConfig(nil)
	assert.NoError(t, err, "Could not call serializeConfig")
	assert.NotNil(t, openVpnConfig, "openVpnConfig should not be nil")
	assert.Contains(t, openVpnConfig, "-----BEGIN RSA PRIVATE KEY-----")
	assert.Contains(t, openVpnConfig, "-----END RSA PRIVATE KEY-----")
	assert.Contains(t, openVpnConfig, "-----BEGIN CERTIFICATE-----")
	assert.Contains(t, openVpnConfig, "-----END CERTIFICATE-----")
	assert.Contains(t, openVpnConfig, "remote")
	assert.Contains(t, openVpnConfig, "remote-cert-tls server")
}
