// introducer is a simple obfuscated proxy that points to an instance of the menshen API.
// The upstream API can be publicly available or not.
package introducer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewIntroducerFromURL(t *testing.T) {
	type args struct {
		introducerURL string
	}
	tests := []struct {
		name    string
		args    args
		want    *Introducer
		wantErr bool
	}{
		{
			name: "working example",
			args: args{"obfsvpnintro://localhost:9090/?cert=87%2BzEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw&kcp=1&fqdn=vpn.example.com"},
			want: &Introducer{
				Type: "obfsvpnintro",
				Addr: "localhost:9090",
				FQDN: "vpn.example.com",
				KCP:  true,
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
			},
			wantErr: false,
		},
		{
			name: "kcp false by default",
			args: args{"obfsvpnintro://localhost:9090/?cert=87%2BzEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw&fqdn=vpn.example.com"},
			want: &Introducer{
				Type: "obfsvpnintro",
				Addr: "localhost:9090",
				FQDN: "vpn.example.com",
				KCP:  false,
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewIntroducerFromURL(tt.args.introducerURL)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseIntroducer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, got, tt.want, "not equal")
		})
	}
}

func TestIntroducer_Validate(t *testing.T) {
	type fields struct {
		Type string
		Addr string
		Cert string
		FQDN string
		KCP  bool
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "happy path",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: false,
		},
		{
			name: "bad type",
			fields: fields{
				Type: "obfsvpnintro2",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "addr with no port",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "example.com",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "no fqdn",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "",
				KCP:  false,
			},
			wantErr: true,
		},
		{
			name: "bad fqdn, no dots",
			fields: fields{
				Type: "obfsvpnintro",
				Addr: "localhost.lan:80",
				Cert: "87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw",
				FQDN: "local",
				KCP:  false,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &Introducer{
				Type: tt.fields.Type,
				Addr: tt.fields.Addr,
				Cert: tt.fields.Cert,
				FQDN: tt.fields.FQDN,
				KCP:  tt.fields.KCP,
			}
			if err := i.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("Introducer.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
