client:
	echo "Fetching latest swagger specification"
	curl https://0xacab.org/leap/menshen/-/raw/main/api/swagger.yaml?inline=false > swagger.yaml
	swagger generate client -c pkg/client -f swagger.yaml
	rm swagger.yaml

test:
	go test -count=1 -v ./... -skip '.*Integration.*'

integration_test:
	sudo docker-compose -f tests/docker-compose.yml up -d --wait
	go test -count=1 -v ./... -run '.*Integration.*'
	sudo docker-compose -f tests/docker-compose.yml down
